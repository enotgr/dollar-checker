import requests
from bs4 import BeautifulSoup

class Currency:
	dollar_rub = 'https://www.google.com/search?q=курс+доллара&oq=курс+долл..'
	# headers = {'User-Agent': 'enter your user agent'}

	def check_currency_price(self):
		full_page = requests.get(self.dollar_rub) #, headers=self.headers)
		soup = BeautifulSoup(full_page.content, 'html.parser')

		convert = soup.findAll("span", {"class": "DFlfde", "class": "SwHCTb", "data-precision": 2})

		return float(convert[0]['data-value'].replace(",", "."))

currency = Currency()